from c2sesb.models import Topic,TopicSource
from django import template
register = template.Library()



@register.filter
def in_topic(id):
    return TopicSource.objects.filter(topic=id)
