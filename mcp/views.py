from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.forms import ValidationError
from c2sesb.models import Topic,TopicSource
from mcp.models import C2SenseUser,Role,Responsability,Team, Severity
from django.views.decorators.csrf import csrf_exempt
from .forms import C2SenseUserForm, RoleForm, ResponsabilityForm,TeamForm, SeverityForm


def index(request):

	title = "C2-SENSE Bus"
	topics =  Topic.objects.filter(value__icontains='rule-')
	return render(request, 'homepage.html', {'title': title, 'topics':topics})



def service_list(request):
	title = "C2-SENSE Bus Services List"
	topics = Topic.objects.filter(value__icontains='service-')

	return render(request, 'service_list.html', {'title': title, 'topics':topics})

def rule_list(request):
	title = "C2-SENSE Rule List"
	topics = Topic.objects.filter(value__icontains='rule-')
	return render(request, 'rule_list.html', {'title': title, 'topics':topics})



def c2senseuser(request):

	title = "C2-SENSE Consumers"
	users = C2SenseUser.objects.all()

	return render(request, 'c2senseuser/index.html', {'title': title, 'users':users})

def role(request):
	title = "C2-SENSE Roles"
	role = Role.objects.all()
	return render(request, 'role/index.html', {'title': title, 'role':role})


def severity(request):
	title = "C2-SENSE Severity"
	severity = Severity.objects.all()
	return render(request, 'severity/index.html', {'title': title, 'severity':severity})

def severitynew(request):
	data = None
	form = SeverityForm(data)
	if request.method == "POST":
		data = request.POST
		form = SeverityForm(data)
		if form.is_valid():
			try:
				severity = form.save(commit=False)
				severity.save()
				return HttpResponseRedirect(reverse('severity_index'))
			except severity.value.DoesNotExist:
				return HttpResponseRedirect(reverse('severity_index'))
	title = "New C2-SENSE Severity"
	return render(request, 'severity/new.html', {'form': form, 'title': title})


def responsabilitynew(request):
	data = None
	form = ResponsabilityForm(data)
	if request.method == "POST":
		data = request.POST
		form = ResponsabilityForm(data)
		if form.is_valid():
			try:
				responsability = form.save(commit=False)
				responsability.save()
				return HttpResponseRedirect(reverse('responsability_index'))
			except responsability.value.DoesNotExist:
				return HttpResponseRedirect(reverse('responsability_index'))
	title = "New C2-SENSE Responsability"
	return render(request, 'responsability/new.html', {'form': form, 'title': title})

def teamnew(request):
	data = None
	form = TeamForm(data)
	if request.method == "POST":
		data = request.POST
		form = TeamForm(data)
		if form.is_valid():
			try:
				form = form.save(commit=False)
				form.save()
				return HttpResponseRedirect(reverse('team_index'))
			except form.value.DoesNotExist:
				return HttpResponseRedirect(reverse('team_index'))
	title = "New C2-SENSE Team"
	return render(request, 'team/new.html', {'form': form, 'title': title})

def rolenew(request):
	data = None
	form = RoleForm(data)
	if request.method == "POST":
		data = request.POST
		form = RoleForm(data)
		if form.is_valid():
			try:
				role = form.save(commit=False)
				role.save()
				return HttpResponseRedirect(reverse('role_index'))
			except c2senseuser.value.DoesNotExist:
				return HttpResponseRedirect(reverse('role_index'))
	title = "New C2-SENSE Role"
	return render(request, 'role/new.html', {'form': form, 'title': title})

def team(request):
	title = "C2-SENSE Teams"
	team = Team.objects.all()
	return render(request, 'team/index.html', {'title': title, 'team':team})

def responsability(request):
	title = "C2-SENSE Responsibilities"
	responsability = Responsability.objects.all()
	return render(request, 'responsability/index.html', {'title': title, 'responsability':responsability})

def c2senseusernew(request):
	data = None
	form = C2SenseUserForm(data)
	if request.method == "POST":
		data = request.POST
		form = C2SenseUserForm(data)
		if form.is_valid():
			try:
				c2sense_user = form.save(commit=False)
				c2sense_user.save()
				form.save_m2m()
				return HttpResponseRedirect(reverse('c2senseuser_index'))
			except c2senseuser.value.DoesNotExist:
				return HttpResponseRedirect(reverse('c2senseuser_index'))
	title = "New C2-SENSE Consumer"
	return render(request, 'c2senseuser/new.html', {'form': form, 'title': title})



def c2senseuseredit(request, id):
	data = None
	if request.method == 'GET':
		try:
			c2senseuser = C2SenseUser.objects.get(pk=id)
			form = C2SenseUserForm(instance=c2senseuser)
			return render(request, 'c2senseuser/edit.html', {'title': c2senseuser.user, 'form': form, 'c2senseuser': c2senseuser})
		except C2SenseUser.DoesNotExist:
			return HttpResponseRedirect(reverse('c2senseuser_index'))
	elif request.method == "POST":
		data = request.POST
		c2senseuser = C2SenseUser.objects.get(pk=id)
		form = C2SenseUserForm(data, instance=c2senseuser)
	if form.is_valid():
		try:
			c2senseuser = form.save(commit=False)
			c2senseuser.save()
			form.save_m2m()
			return HttpResponseRedirect(reverse('c2senseuser_index'))
		except C2SenseUser.DoesNotExist:
			return HttpResponseRedirect(reverse('c2senseuser_index'))
