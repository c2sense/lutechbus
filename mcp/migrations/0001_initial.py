# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('c2sesb', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='C2SenseUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('facebook', models.CharField(default=None, max_length=200, blank=True)),
                ('twitter', models.CharField(default=None, max_length=200, blank=True)),
                ('email', models.CharField(default=None, max_length=200, blank=True)),
                ('mobile', models.CharField(default=None, max_length=200, blank=True)),
                ('fax', models.CharField(default=None, max_length=200, blank=True)),
                ('topic', models.ManyToManyField(to='c2sesb.Topic')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
