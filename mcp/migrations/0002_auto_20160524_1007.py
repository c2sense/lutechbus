# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mcp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Responsability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(default=None, max_length=200, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(default=None, max_length=200, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(default=None, max_length=200, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='c2senseuser',
            name='responsability',
            field=models.ForeignKey(default=None, blank=True, to='mcp.Responsability', null=True),
        ),
        migrations.AddField(
            model_name='c2senseuser',
            name='role',
            field=models.ForeignKey(default=None, blank=True, to='mcp.Role', null=True),
        ),
        migrations.AddField(
            model_name='c2senseuser',
            name='team',
            field=models.ForeignKey(default=None, blank=True, to='mcp.Team', null=True),
        ),
    ]
