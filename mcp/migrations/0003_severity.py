# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mcp', '0002_auto_20160524_1007'),
    ]

    operations = [
        migrations.CreateModel(
            name='Severity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(default=None, max_length=200, blank=True)),
            ],
        ),
    ]
