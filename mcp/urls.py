from django.conf.urls import url
from django.conf.urls import patterns, url


from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^c2senseuser$', views.c2senseuser, name='c2senseuser_index'),
    url(r'^c2senseusernew$', views.c2senseusernew, name='c2senseuser_new'),
    url(r'^c2senseuseredit/(\d+)/$', views.c2senseuseredit, name='c2senseuser_edit'),
    url(r'^role$', views.role, name='role_index'),
    url(r'^rolenew$', views.rolenew, name='role_new'),
    url(r'^responsability$', views.responsability, name='responsability_index'),
    url(r'^responsabilitynew$', views.responsabilitynew, name='responsability_new'),
    url(r'^team$', views.team, name='team_index'),
    url(r'^teamnew$', views.teamnew, name='team_new'),
    url(r'^service_list$', views.service_list, name='service_list_index'),
    url(r'^rule_list$', views.rule_list, name='rule_list_index'),
    url(r'^severity$', views.severity, name='severity_index'),
    url(r'^severitynew$', views.severitynew, name='severity_new'),
]
