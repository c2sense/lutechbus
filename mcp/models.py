from django.db import models
from django.contrib.auth.models import User




class Role(models.Model):
    description = models.CharField(max_length=200, default=None, blank=True)
    def __unicode__(self):
        return u"{0}".format(self.description)


class Responsability(models.Model):
    description = models.CharField(max_length=200, default=None, blank=True)
    def __unicode__(self):
        return u"{0}".format(self.description)

class Team(models.Model):
    description = models.CharField(max_length=200, default=None, blank=True)
    def __unicode__(self):
        return u"{0}".format(self.description)

class Severity(models.Model):
    description = models.CharField(max_length=200, default=None, blank=True)
    def __unicode__(self):
        return u"{0}".format(self.description)

class C2SenseUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    facebook = models.CharField(max_length=200, default=None, blank=True)
    twitter = models.CharField(max_length=200, default=None, blank=True)
    email = models.CharField(max_length=200, default=None, blank=True)
    mobile = models.CharField(max_length=200, default=None, blank=True)
    fax = models.CharField(max_length=200, default=None, blank=True)
    topic = models.ManyToManyField('c2sesb.Topic')
    role = models.ForeignKey(Role, null=True, blank=True, default = None)
    team = models.ForeignKey(Team, null=True, blank=True, default = None)
    responsability = models.ForeignKey(Responsability, null=True, blank=True, default = None)
