$(document).ready( function () {
  $('.dtable').dataTable({   
    });

  $('.dtable2').dataTable({
     "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
   "sPaginationType": "bootstrap",
      "bPaginate": false,
      "bFilter": false,
      "info": false
    }); 


var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData = {
      labels : ["January","February","March","April","May","June","July"],
      datasets : [
        {
          label: "My First dataset",
          fillColor : "rgba(220,220,220,0.2)",
          strokeColor : "rgba(220,220,220,1)",
          pointColor : "rgba(220,220,220,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(220,220,220,1)",
          data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
        },
        {
          label: "My Second dataset",
          fillColor : "rgba(151,187,205,0.2)",
          strokeColor : "rgba(151,187,205,1)",
          pointColor : "rgba(151,187,205,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(151,187,205,1)",
          data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
        }
      ]
    }




  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');

  $(document).ready(function() {
      $('.has-popover').popover({'trigger':'hover'});
  });

  $('#check').click(function(){
    dictionary = $("#dictionary option:selected").val()
    sentence = $("#sentence").val()


    function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
      }

    $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                  }
                });
    $.ajax({
          type: "POST",
          url: "/engine/check_dictionary_ajax",
          data: "dictionary=" + dictionary + "&sentence=" + sentence,
          dataType: "html",
          success: function(msg)
          {
            var json_response = jQuery.parseJSON( msg );
            $('#word_number').html(json_response.word_number);
            $('#match').html(json_response.match);
            $('#severity').html(json_response.severity);
            intelligence_goal = ""
            $('#regexp_area').hide();
            for (var i in json_response.tag_list.intelligence_goal) 
              {
                  intelligence_goal += '<span class="label label-danger">'+json_response.tag_list.intelligence_goal[i]+'</span>'+' ';
              }
            $('#intelligence_goal').html(intelligence_goal);

            regexp = ""
            for (var i in json_response.tag_list.regexp) 
              {   
                  $('#regexp_area').show();
                  regexp += json_response.tag_list.regexp[i]+'; ';
              }
            $('#regexp').val(regexp)
            threat_category = ""
            for (var i in json_response.tag_list.threat_category) 
              {
                  threat_category += '<span class="label label-warning">'+json_response.tag_list.threat_category[i]+'</span>'+' ';
              }
            $('#threat_category').html(threat_category);
          },
          error: function()
          {
            alert("Chiamata fallita, si prega di riprovare...");
          }
        });

} );
  } );


$("#tag-incident").on('itemAdded', function(event) {
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');

  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
      }

  $.ajaxSetup({
          beforeSend: function(xhr, settings) {
              if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                  xhr.setRequestHeader("X-CSRFToken", csrftoken);
              }
            }
          });
    $.ajax({
          type: "POST",
          url: "/event/ajax_save_tag",
          data: "tag=" + event.item+"&event_id=" + $("#event_id").html(),
          dataType: "html",
          success: function()
          {
            
          },
          error: function()
          {
            alert("Chiamata fallita, si prega di riprovare...");
          }
        });

} );


$("#tag-incident").on('itemRemoved', function(event) {
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');


  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
      }

  $.ajaxSetup({
          beforeSend: function(xhr, settings) {
              if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                  xhr.setRequestHeader("X-CSRFToken", csrftoken);
              }
            }
          });
    $.ajax({
          type: "POST",
          url: "/event/ajax_remove_tag",
          data: "tag=" + event.item+"&event_id=" + $("#event_id").html(),
          dataType: "html",
          success: function()
          {
            
          },
          error: function()
          {
            alert("Chiamata fallita, si prega di riprovare...");
          }
        });

} );


$('.attach-detail').click(function(){
    obj_id = $(this).attr("data-obj-id")

    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
      }

    $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                  }
                });
    $.ajax({
          type: "POST",
          url: "/event/ajax_attachment_details",
          data: "obj_id=" + obj_id,
          dataType: "html",
          success: function(msg)
          {

            var json_response = jQuery.parseJSON( msg );
            $('#id').html(json_response.id);
            $('#magictype').html(json_response.magic_type);
            $('#md5').html(json_response.md5);
            $('#sha256').html(json_response.sha256);
            $('#description').html(json_response.description);
            $('#value').html(json_response.value);

          },
          error: function()
          {
            alert("Chiamata fallita, si prega di riprovare...");
          }
        });

} );
