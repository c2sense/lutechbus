from django.forms import ModelForm
from .models import C2SenseUser,Role,Responsability,Team,Severity
from c2sesb.models import Topic,TopicSource

class C2SenseUserForm(ModelForm):
    class Meta:
        model = C2SenseUser
        fields = ['user','role','team','responsability','facebook','twitter','email','mobile','fax','topic']
    def __init__(self, *args, **kwargs):
        super(C2SenseUserForm, self).__init__(*args, **kwargs)
        self.fields['topic'].queryset = Topic.objects.filter(value__icontains='rule-')


class RoleForm(ModelForm):
    class Meta:
        model = Role
        fields = ['description']

class SeverityForm(ModelForm):
    class Meta:
        model = Severity
        fields = ['description']

class TeamForm(ModelForm):
    class Meta:
        model = Team
        fields = ['description']

class ResponsabilityForm(ModelForm):
    class Meta:
        model = Responsability
        fields = ['description']
