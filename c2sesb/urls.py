from django.conf.urls import url
from django.conf.urls import patterns, url
from c2sesb.views import RuleManagerService, ServiceManagerService


from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView


from . import views

urlpatterns = [
    url(r'^rule_manager/', DjangoView.as_view(
        services=[RuleManagerService], tns='c2sense.bus.c2sesb',
        in_protocol=Soap11(validator='lxml'), out_protocol=Soap11())),

    url(r'^service_manager/', DjangoView.as_view(
        services=[ServiceManagerService], tns='c2sense.bus.c2sesb',
        in_protocol=Soap11(validator='lxml'), out_protocol=Soap11())),
]
