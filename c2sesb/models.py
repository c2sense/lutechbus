from django.db import models

class Topic(models.Model):
    value = models.CharField(max_length=200)
    def __unicode__(self):
        return u"{0}".format(self.value)


class TopicSource(models.Model):
    topic = models.ForeignKey(Topic)
    key = models.CharField(max_length=200, default=None, blank=True)
    value = models.CharField(max_length=200, default=None, blank=True)
    def __unicode__(self):
        return u"{0}".format(self.id)


class Group(models.Model):
    topic = models.ForeignKey(Topic)
    offset = models.IntegerField(default=0, blank=True)
    name = models.CharField(max_length=200, default=None, blank=True)
    def __unicode__(self):
        return u"{0}".format(self.id)
