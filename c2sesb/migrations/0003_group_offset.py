# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('c2sesb', '0002_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='offset',
            field=models.IntegerField(default=0, blank=True),
        ),
    ]
