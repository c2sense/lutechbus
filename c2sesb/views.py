from django.shortcuts import render
from spyne import Application, rpc, ServiceBase, Iterable, Integer, Unicode, ComplexModel, String, Array, Boolean,ByteArray
from spyne.error import ResourceNotFoundError, ResourceAlreadyExistsError
from spyne.server.django import DjangoApplication
from spyne.model.primitive import Unicode, Integer
from spyne.model.complex import Iterable
from spyne.service import ServiceBase
from spyne.protocol.soap import Soap11
from spyne.application import Application
from spyne.decorator import rpc
from spyne.error import *
from spyne.util.django import DjangoComplexModel, DjangoServiceBase
from django.views.decorators.csrf import csrf_exempt
from kafka import SimpleProducer, KafkaClient, KafkaConsumer,SimpleConsumer
from kafka.protocol.offset import OffsetRequest, OffsetResetStrategy
from kafka.common import OffsetRequestPayload
import logging
from random import randint
from lxml import etree
import base64
import io
import re
from c2sesb.models import Topic,TopicSource,Group
from django.core.exceptions import ObjectDoesNotExist

logging.basicConfig(level=logging.DEBUG)

Base64 = String(pattern='[0-9a-zA-Z/+=]+')

# Server 198.245.63.40
# max buffer size = 9932768

def get_last_offset(client,topic_id):
    partitions = client.topic_partitions[topic_id]
    offset_requests = [OffsetRequestPayload(topic_id, p, -1, 1) for p in partitions.keys()]
    offsets_responses = client.send_offset_request(offset_requests)
    offset = 0
    for r in offsets_responses:
        offset = r.offsets[0]
    pass
    return offset

def create_topic(id):
    kafka = KafkaClient('localhost:9092')
    producer = SimpleProducer(kafka, async = False)
    producer.send_messages(id, base64.b64encode("Topic Init"))

class SourceMapItem(ComplexModel):
    __namespace__ = "esb"
    key = String()
    value = String()

class Message(ComplexModel):
    __namespace__ = "esb"
    message = String()


class SourceMap(ComplexModel):
    __namespace__ = "esb"
    source_map = Array(SourceMapItem)


class MessageListItem(ComplexModel):
    __namespace__ = "esb"
    message = String()

class MessageList(ComplexModel):
    __namespace__ = "esb"
    message_list = Array(MessageListItem)
    source_map = Array(SourceMap)

class RuleManagerService(ServiceBase):
    # OK
    @rpc(SourceMap, _returns=String)
    def addRule(ctx, source_data, topic_name = ''):
        if len(topic_name) == 0 : 
            topic_name = "rule-" + str(randint(0,99999))
        topic_id = Topic.objects.create(value = topic_name)
        create_topic(topic_name)
        for i in source_data[0]:
            TopicSource.objects.create(topic = topic_id,key=str(i.key),value=str(i.value))
        return topic_name
    # OK
    @rpc(String(nillable=False,min_occurs=1),SourceMap, _returns=Boolean)
    def modifyRule(ctx, topic_id, source_data):
        topic = Topic.objects.get(value=topic_id)
        TopicSource.objects.filter(topic=topic).delete()
        for i in source_data[0]:
            TopicSource.objects.create(topic = topic,key=str(i.key),value=str(i.value))
        return True
    # OK
    @rpc(String(nillable=False,min_occurs=1), _returns=Boolean)
    def deleteRule(ctx, topic_id):
        topic = Topic.objects.get(value=topic_id).id
        Topic.objects.filter(value=topic_id).delete()
        TopicSource.objects.filter(topic_id=topic).delete()
        return True


class ServiceManagerService(ServiceBase):
    # OK
    @rpc(_returns=String)
    def createService(ctx):
        topic_name = "service-" + str(randint(0,99999))
        topic_id = Topic.objects.create(value = topic_name)
        create_topic(topic_name)
        if topic_id is None:
            raise InternalError("Error in the creation of the Service")
        else:
            return topic_name
    # OK
    @rpc(String(nillable=False,min_occurs=1), _returns=Boolean)
    def deleteService(ctx, topic_id):
        a = Topic.objects.filter(value=topic_id)
        if not a:
            raise InvalidInputError(faultstring='The provided topic code does not exists', data=topic_id)
        else:
            l = a.delete()
            return True
    # OK
    @rpc(_returns=ByteArray)
    def viewServices(ctx):
        root = etree.Element('servicelist')
        all_topics = Topic.objects.all()
        for tpc in all_topics:
            service = etree.Element('service')
            root.append(service)
            topic = etree.Element('topicid')
            topic.text = str(tpc.value)
            service.append(topic)
            pass
        result = etree.tostring(root, pretty_print=True)
        if not result:
            raise InternalError("Error occured while retrieving the services")
        else:
            return result

    @rpc(String(nillable=False,min_occurs=1), _returns=ByteArray)
    def viewSourceData(ctx, topic_id):
        try:
            topic_id = Topic.objects.get(value=topic_id)
        except Topic.DoesNotExist:
            raise InvalidInputError(faultstring='The provided topic code does not exists', data=topic_id)
        topic_source = TopicSource.objects.filter(topic=topic_id)
        root = etree.Element('sourceData')
        topic = etree.Element('topicid')
        topic.text = str(topic_id)
        root.append(topic)
        sourcelist = etree.Element('sourcelist')
        root.append(sourcelist)
        for ts in topic_source:
            key = etree.Element('Key')
            key.text = str(ts.key)
            sourcelist.append(key)
            value = etree.Element('Value')
            value.text = str(ts.value)
            sourcelist.append(value)
        result = etree.tostring(root, pretty_print=True)
        if not result:
            raise InternalError("Error occured while retrieving the services")
        else:
            return result

    # OK
    @rpc(String(nillable=False,min_occurs=1),ByteArray(nillable=False,min_occurs=1), _returns=Boolean)
    def sendMessage(ctx, topic_id, message):
        kafka = KafkaClient('localhost:9092')
        producer = SimpleProducer(kafka, async = False)
        encoded = base64.b64encode(message[0])
        producer.send_messages(str(topic_id), str(encoded))
        return True

    # OK
    @rpc(Integer(nillable=False,min_occurs=1), String(nillable=False,min_occurs=1), _returns=MessageList)
    def getMessages(ctx,lenght, topic_id):
        group = "group-" + str(randint(0,99999))
        c = KafkaClient('localhost:9092')
        cons = SimpleConsumer(c, group, str(topic_id),max_buffer_size=1500000,auto_commit_every_t=1000)
        last_offset = int(get_last_offset(c,topic_id))

        # The first input Length specifies the number of messages to acquire from the topic starting from the last one
        # (from newest to oldest). If Lenght is set to 0 or to other
        # not positive values or exceeds the number of available messages, the function returns last 5 messages at maximum.

        if (last_offset < lenght) or (lenght <= 0):
            lenght = 5
            if (last_offset < lenght):
                lenght = last_offset

        cons.seek(offset=-lenght,whence=2)
        messages = cons.get_messages(count=lenght,block=False)
        try:
            topic_id = Topic.objects.get(value=topic_id)
        except Topic.DoesNotExist:
            raise InvalidInputError(faultstring='The provided topic code does not exists', data=topic_id)

        topic_source = TopicSource.objects.filter(topic=topic_id)
        source_map = SourceMap()
        source_map_array = []
        source_map_a = []

        for ts in topic_source:
            source_map_item = SourceMapItem()
            source_map_item.key = ts.key
            source_map_item.value = ts.value
            source_map_array.append(source_map_item)
        source_map.source_map = source_map_array
        source_map_a.append(source_map)

        cont = 0
        msg_list = []
        messagelist = MessageList()
        result = []
        for msg in messages:
            cleaned_msg = msg[1].value
            msg_list.append(cleaned_msg)
            cont = cont + 1
        for i in range(0,lenght):
            if msg_list:
                msg2 = Message()
                msg2.message = msg_list.pop()
                result.append(msg2)
        messagelist.message_list = result
        messagelist.source_map = source_map_a
        return messagelist

    # OK
    @rpc(Integer(nillable=False,min_occurs=1), String(nillable=False,min_occurs=1), String(nillable=False,min_occurs=1), _returns=MessageList)
    def getMessagesFromGroup(ctx,lenght, topic_id, group_id):
        group = str(group_id)
        c = KafkaClient('localhost:9092')
        # Retrieving the max offeset for a given topic
        last_offset = int(get_last_offset(c,topic_id))
        # The first input Length specifies the number of messages to acquire from the topic starting from the last one
        # (from newest to oldest). If Lenght is set to 0 or to other
        # not positive values or exceeds the number of available messages, the function returns last 5 messages at maximum.
        if (last_offset < lenght) or (lenght <= 0):
            lenght = 5
        try:
            # Check if the topic exists.
            topic = Topic.objects.get(value=topic_id)
        except Topic.DoesNotExist:
            raise InvalidInputError(faultstring='The provided topic code does not exists', data=topic_id)
            # Check if the (group+topic) exists.
        if not Group.objects.filter(topic=topic,name=group):
            cons = SimpleConsumer(c, group, str(topic_id),max_buffer_size=1500000,auto_commit_every_t=1000)
            # Check if the number of requested messages in bigger than the last_offset
            if lenght < last_offset:
                # offset is the -(number of requested messages) lenght.
                # whence 2 is relative to the latest known offset (tail)
                cons.seek(offset=-lenght,whence=2)
            else:
                cons.seek(offset=-last_offset,whence=2)
            messages = cons.get_messages(count=lenght,block=False)
            Group.objects.create(topic=topic,name=group,offset=last_offset)
        else:
            offset = Group.objects.get(topic=topic,name=group).offset
            cons = SimpleConsumer(c, group, str(topic_id),max_buffer_size=1500000,auto_commit_every_t=1000)
            if offset == last_offset:
                messages = cons.get_messages(count=lenght,block=False)
            else:
                current_offset = last_offset - offset
                if lenght < current_offset:
                    cons.seek(offset=-lenght,whence=2)
                else:
                    cons.seek(offset=-current_offset,whence=2)
                messages = cons.get_messages(count=lenght,block=False)
                group = Group.objects.filter(topic=topic,name=group)
                group.update(offset=last_offset)

        topic_id = topic
        topic_source = TopicSource.objects.filter(topic=topic_id)
        source_map = SourceMap()
        source_map_array = []
        source_map_a = []
        for ts in topic_source:
            source_map_item = SourceMapItem()
            source_map_item.key = ts.key
            source_map_item.value = ts.value
            source_map_array.append(source_map_item)
        source_map.source_map = source_map_array
        source_map_a.append(source_map)
        cont = 0
        msg_list = []
        messagelist = MessageList()
        result = []
        for msg in messages:
            cleaned_msg = msg[1].value
            msg_list.append(cleaned_msg)
            cont = cont + 1
        for i in range(0,lenght):
            if msg_list:
                msg2 = Message()
                msg2.message = msg_list.pop()
                result.append(msg2)
        messagelist.message_list = result
        messagelist.source_map = source_map_a
        return messagelist

app = Application([RuleManagerService, ServiceManagerService],
    'c2sense.bus.c2sesb',
    in_protocol=Soap11(validator='lxml'),
    out_protocol=Soap11(),
)

rule_manager_service = csrf_exempt(DjangoApplication(app))




### OLd get messages
#consumer = KafkaConsumer(str(topic_id), # Nome del topic
#                  group_id = str(group_id),
#                  auto_offset_reset='earliest', # Questa opzione fa partire dall'inizio l'offset
#                  consumer_timeout_ms=3000,
#                  bootstrap_servers=['198.245.63.40:9092'])
#
# topic_id = Topic.objects.get(value=topic_id)
# topic_source = TopicSource.objects.filter(topic=topic_id)
#
# source_map = SourceMap()
# source_map_array = []
# source_map_a = []
#
# for ts in topic_source:
#     source_map_item = SourceMapItem()
#     source_map_item.key = ts.key
#     source_map_item.value = ts.value
#     source_map_array.append(source_map_item)
# source_map.source_map = source_map_array
# source_map_a.append(source_map)
#
# cont = 0
# msg_list = []
# messagelist = MessageList()
# result = []
# for msg in consumer:
#     cleaned_msg = msg[4]
#     msg_list.append(cleaned_msg)
#     cont = cont + 1
# for i in range(0,lenght):
#     if msg_list:
#         msg2 = Message()
#         msg2.message = msg_list.pop()
#         result.append(msg2)
# messagelist.message_list = result
# messagelist.source_map = source_map_a
# return messagelist
