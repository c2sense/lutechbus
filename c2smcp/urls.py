from django.conf.urls import url
from django.conf.urls import patterns, url
from c2smcp.views import Consumer, Brokerage


from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView


from . import views

urlpatterns = [
    url(r'^consumer/', DjangoView.as_view(
        services=[Consumer], tns='c2sense.bus.c2smcp',
        in_protocol=Soap11(validator='lxml'), out_protocol=Soap11())),

    url(r'^brokerage/', DjangoView.as_view(
        services=[Brokerage], tns='c2sense.bus.c2smcp',
        in_protocol=Soap11(validator='lxml'), out_protocol=Soap11())),
]
