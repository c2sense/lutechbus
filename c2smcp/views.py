from django.shortcuts import render
from spyne import Application, rpc, ServiceBase, Iterable, Integer, Unicode, ComplexModel, String, Array, Boolean,ByteArray
from spyne.error import ResourceNotFoundError, ResourceAlreadyExistsError
from spyne.server.django import DjangoApplication
from spyne.model.primitive import Unicode, Integer
from spyne.model.complex import Iterable
from spyne.service import ServiceBase
from spyne.protocol.soap import Soap11
from spyne.application import Application
from spyne.decorator import rpc
from spyne.util.django import DjangoComplexModel, DjangoServiceBase
from django.views.decorators.csrf import csrf_exempt
from kafka import SimpleProducer, KafkaClient, KafkaConsumer,SimpleConsumer
from kafka.protocol.offset import OffsetRequest, OffsetResetStrategy
from kafka.common import OffsetRequestPayload
import logging
from random import randint
from lxml import etree
import base64
import io
from c2smcp.models import Role, RoleLevel
from c2smcp.models import Consumer as ConsumerModel
from c2sesb.models import Topic,TopicSource, Group
from django.core.exceptions import ObjectDoesNotExist


def get_last_offset(client,topic):

    partitions = client.topic_partitions[topic.value]
    offset_requests = [OffsetRequestPayload(topic.value, p, -1, 1) for p in partitions.keys()]
    offsets_responses = client.send_offset_request(offset_requests)
    offset = 0
    for r in offsets_responses:
        offset = r.offsets[0]
    pass
    return offset

class SourceMapItem(ComplexModel):
    __namespace__ = "esb"
    key = String()
    value = String()

class Message(ComplexModel):
    __namespace__ = "esb"
    message = String()


class SourceMap(ComplexModel):
    __namespace__ = "esb"
    source_map = Array(SourceMapItem)


class MessageListItem(ComplexModel):
    __namespace__ = "esb"
    message = String()

class MessageList(ComplexModel):
    __namespace__ = "esb"
    message_list = Array(MessageListItem)
    source_map = Array(SourceMap)

class Consumer(ServiceBase):
    @rpc(String(nillable=False,min_occurs=1),String(nillable=False,min_occurs=1), _returns=Array(String))
    def addConsumer(ctx, topic_id, consumer_id):
        if not consumer_id:
            consumer_id = str(randint(0,999999999))
        topic_id = Topic.objects.get(value=topic_id)
        if topic_id:
            group_id = randint(0,999999999)
            ConsumerModel.objects.create(consumer = consumer_id, topic = topic_id ,group=group_id)
        else:
            return 0
        return [group_id,consumer_id]

    @rpc(String(nillable=False,min_occurs=1), _returns=Integer)
    def createRole(ctx, role_type):
        role_id = Role.objects.create(role_type="role_type")
        if role_id:
            return role_id.id
        else:
            raise InvalidInputError(faultstring='The provided role_type is not correct', data=role_type)


    @rpc(String(nillable=False,min_occurs=1),String(nillable=False,min_occurs=1),String(nillable=False,min_occurs=1),String(nillable=False,min_occurs=1), _returns=Boolean)
    def assignRole(ctx, consumer_id,role_id,topic_id,level):
        topic_id = Topic.objects.get(value=topic_id)
        consumer_id = ConsumerModel.objects.get(consumer=consumer_id)
        role_id = Role.objects.get(pk=role_id)
        if topic_id and consumer_id and role_id:
            RoleLevel.objects.create(role_type=role_id, consumer_id=consumer_id, topic=topic_id, level=level)
            return True
        else:
            raise InvalidInputError(faultstring='An error occured', data=consumer_id)

    @rpc(Integer,Integer, _returns=Boolean)
    def unsubscribeFromGroup(ctx, consumer_id,group_id):
        ConsumerModel.objects.filter(consumer=consumer_id,group=group_id).delete()
        return True


    @rpc(Integer, _returns=Boolean)
    def unsubscribeFromTopic(ctx, topic_id):
        if topic_id:
            topic_id = Topic.objects.get(value=topic_id)
            ConsumerModel.objects.filter(topic = topic_id).delete()
            return True
        else:
            raise InvalidInputError(faultstring='The provided topic code does not exists', data=topic_id)


class Brokerage(ServiceBase):
    @rpc(String,String,ByteArray, _returns=Boolean)
    def sendMessage(ctx, consumer_id,topic_id, document):
        topic_id_real = Topic.objects.get(value=topic_id)
        group = ConsumerModel.objects.get(consumer=consumer_id,topic = topic_id).group
        consumer_id = ConsumerModel.objects.get(consumer=consumer_id)
        level = RoleLevel.objects.get(consumer_id=consumer_id,topic=topic_id_real).level

        if level == "1" or level == "11":
            kafka = KafkaClient('localhost:9092')
            producer = SimpleProducer(kafka, async = False)
            encoded = base64.b64encode(message[0])
            producer.send_messages(str(topic_id), str(encoded))
        return True



    @rpc(String,String,Integer, _returns=MessageList)
    def receiveMessage(ctx, consumer_id, group_id, lenght):
        topic_id = ConsumerModel.objects.get(consumer = consumer_id,group=group_id).topic
        consumer_id = ConsumerModel.objects.get(consumer=consumer_id)
        level = RoleLevel.objects.get(consumer_id=consumer_id,topic=topic_id).level
        if level == "11" or level == "10":
            group = str(group_id)
            c = KafkaClient('localhost:9092')
            last_offset = int(get_last_offset(c,topic_id))
            # The first input Length specifies the number of messages to acquire from the topic starting from the last one
            # (from newest to oldest). If Lenght is set to 0 or to other
            # not positive values or exceeds the number of available messages, the function returns last 5 messages at maximum.
            if (last_offset < lenght) or (lenght <= 0):
                lenght = 5

            try:
                topic = Topic.objects.get(value=topic_id)
            except Topic.DoesNotExist:
                raise InvalidInputError(faultstring='The provided topic code does not exists', data=topic_id)
            if not Group.objects.filter(topic=topic,name=group):
                cons = SimpleConsumer(c, group, str(topic_id),max_buffer_size=9932768,auto_commit_every_t=1000)
                if lenght < last_offset:
                    cons.seek(offset=-lenght,whence=2)
                else:
                    cons.seek(offset=-last_offset,whence=2)
                messages = cons.get_messages(count=lenght,block=False)
                Group.objects.create(topic=topic,name=group,offset=last_offset)
            else:
                offset = Group.objects.get(topic=topic,name=group).offset
                cons = SimpleConsumer(c, group, str(topic_id),max_buffer_size=9932768,auto_commit_every_t=1000)
                if offset == last_offset:
                    messages = cons.get_messages(count=lenght,block=False)
                else:
                    current_offset = last_offset - offset
                    if lenght < current_offset:
                        cons.seek(offset=-lenght,whence=2)
                    else:
                        cons.seek(offset=-current_offset,whence=2)
                    messages = cons.get_messages(count=lenght,block=False)
                    group = Group.objects.filter(topic=topic,name=group)
                    group.update(offset=last_offset)
            topic_id = topic
            topic_source = TopicSource.objects.filter(topic=topic_id)
            source_map = SourceMap()
            source_map_array = []
            source_map_a = []
            for ts in topic_source:
                source_map_item = SourceMapItem()
                source_map_item.key = ts.key
                source_map_item.value = ts.value
                source_map_array.append(source_map_item)
            source_map.source_map = source_map_array
            source_map_a.append(source_map)
            cont = 0
            msg_list = []
            messagelist = MessageList()
            result = []
            for msg in messages:
                cleaned_msg = msg[1].value
                msg_list.append(cleaned_msg)
                cont = cont + 1
            for i in range(0,lenght):
                if msg_list:
                    msg2 = Message()
                    msg2.message = msg_list.pop()
                    result.append(msg2)
            messagelist.message_list = result
            messagelist.source_map = source_map_a
            return messagelist


app = Application([Consumer,Brokerage],
    'c2sense.bus.c2smcp',
    in_protocol=Soap11(validator='lxml'),
    out_protocol=Soap11(),
)

consumer = csrf_exempt(DjangoApplication(app))
