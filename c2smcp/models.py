from django.db import models

class Consumer(models.Model):
    consumer = models.CharField(max_length=200, default=None, blank=True)
    topic = models.ForeignKey('c2sesb.Topic')
    group = models.CharField(max_length=200, default=None, blank=True)
    def __unicode__(self):
        return u"{0}".format(self.id)

class Role(models.Model):
    role_type = models.CharField(max_length=200, default=None, blank=True)


class RoleLevel(models.Model):
    role_type = models.ForeignKey(Role)
    consumer_id = models.ForeignKey(Consumer)
    topic = models.ForeignKey('c2sesb.Topic')
    level = models.CharField(max_length=2, default=None, blank=True)
