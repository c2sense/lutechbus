# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('c2sesb', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Consumer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('consumer', models.CharField(default=None, max_length=200, blank=True)),
                ('group', models.CharField(default=None, max_length=200, blank=True)),
                ('topic', models.ForeignKey(to='c2sesb.Topic')),
            ],
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role_type', models.CharField(default=None, max_length=200, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='RoleLevel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.CharField(default=None, max_length=2, blank=True)),
                ('consumer_id', models.ForeignKey(to='c2smcp.Consumer')),
                ('role_type', models.ForeignKey(to='c2smcp.Role')),
                ('topic', models.ForeignKey(to='c2sesb.Topic')),
            ],
        ),
    ]
