from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login,logout

urlpatterns = [
    # Examples:
    # url(r'^$', 'lutechbus.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', login),
    url(r'^logout/$', logout, {'next_page': '/login/'}, name='logout'),
    url(r'^mcp/', include('mcp.urls')),
    url(r'^c2sesb/', include('c2sesb.urls')),
    url(r'^c2smcp/', include('c2smcp.urls')),
]
