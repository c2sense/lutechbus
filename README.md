# **Lutech Bus** #


This *tutorial* provides all the information for installing the Lutech Bus.


Needed tools:

```
#!bash
* Up and Running Kafka+Zookeper;
* Python 2.7.x;
* Debian like OS, better Ubuntu;
* Python PIP;
```

## Clone the repository of lutechbus in a repository: ##

```
#!bash

$ git clone https://Lutech-C2SENSE@bitbucket.org/c2sense/lutechbus.git

```

## Install all the needed python libraries: ##  

```
#!bash
# In particular make sure these are already installed:
sudo apt-get install libmysqlclient-dev python-dev

# Inside the root directory of the project:
$ pip install -r requirements.txt

```

## Migrate the database, it will create a local sqlite.db file: ##



```
#!bash
# Inside the root directory of the project:
$ python manage.py makemigrations
$ python manage.py migrate

```
## Run the server: ##

```
#!bash
# Inside the root directory of the project:
$ python manage.py runserver
```
## Check this:
```
http://127.0.0.1:8000/c2sesb/service_manager/
```

## Check the addresses of kafka instance inside: ##


```
#!bash
# Change the values of the address to the one of your kafka instance.
c2sesb/views.py
c2smcp/views.py

```